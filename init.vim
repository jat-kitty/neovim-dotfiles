" vim:fdm=marker:fml=1:noexpandtab:ts=4

" autoload ---------------------------------------------------------- {{{1
execute pathogen#infect()
source C:\\Users\\ratta\\.config\\nvim\\autoload\\colemak.vim

" the normal stuff -------------------------------------------------- {{{1
language en_US.UTF8
filetype plugin indent on
syntax on

set autoread

set number
set ruler

set scrolloff=7
set sidescrolloff=5

set wildmenu
set wildignore=*.o,*~,*/.git/*,*/target/*

set hid

set ignorecase smartcase
set showmatch mat=2

set lazyredraw

set magic

set belloff="error"
set visualbell
set tm=500

set foldmethod=indent
set foldcolumn=4
set foldminlines=5
set foldlevel=0

set modelines=2
set noshowmode
set laststatus=2
set list
set listchars=eol:¬,tab:»\ ,trail:·

set nobackup writebackup
set backupdir=C:\\Users\\ratta\\OneDrive\\Dokumente\\.backup\\
set swapfile
set directory=.,C:\\Users\\ratta\\OneDrive\\Dokumente\\.backup\\

set tabstop=4
set shiftwidth=4
set softtabstop=0
set noexpandtab

set tw=500

set encoding=utf8
set ffs=unix,dos,mac

" autocommands ------------------------------------------------------ {{{1
" BEHOLD i'm going to use abreviations of the commands since I'm lazy

augroup __rust
	autocmd!
	autocmd FileType rust set noet ts=4 sw=4
	autocmd FileType rust set fdm=syntax
augroup END

augroup __votl
	autocmd!
	autocmd FileType votl set fdc=7 fml=5
	autocmd FileType votl set listchars=eol:¬,tab:\ \ ,trail:·
augroup END

" mapping and shortcuts and whatever -------------------------------- {{{1
" btw: i'm using colemak :p
let mapleader = ","

nnoremap . <nop>
nnoremap .. .

nnoremap <leader>w :write!<return>

nnoremap ; :

nnoremap <space> za

inoremap <esc> <nop>
inoremap ne <esc>
inoremap en <esc>

nnoremap <left> <c-w><left>
nnoremap <right> <c-w><right>
nnoremap <up> <c-w><up>
nnoremap <down> <c-w><down>

" interface related shit -------------------------------------------- {{{1
let base16colorspace=256
set termguicolors
colorscheme nord
set background=dark

" lightline {{{2
let g:lightline = {
\	'colorscheme': 'nord',
\	'active': {
\		'left': [
\			['mode', 'paste'],
\			['gitbranch'],
\			['readonly', 'filename', 'modified']
\		],
\		'right': [
\			['lineinfo', 'percent'],
\			['filetype', 'fileformat', 'fileencoding']
\		]
\	},
\	'inactive': {
\		'left': [
\			['filename', 'modified']
\		],
\		'right': [
\			['percent'],
\			['filetype']
\		]
\ 	},
\	'component_function': {
\		'gitbranch': 'fugitive#head'
\	},
\	'mode_map': {
\		'n': 'normal',
\		'i': 'insert',
\		'R': 'replace',
\		'v': 'visual',
\		'V': 'v-line',
\		"\<c-v>": 'v-block',
\		'c': 'exec',
\		's': 'select',
\		'S': 's-line',
\		"\<c-s>": 's-block',
\		't': 'terminal'
\	},
\ } " }}}2

" quality of shit --------------------------------------------------- {{{1
" these settings are just plain quality of life settings like notes or syntaxchecking

" CtrlP {{{2
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'rwa'
let g:ctrlp_user_command ='ag %s -l --nocolor --hidden -g ""'
let g:ctrlp_regexp = 1

" Vim Pad {{{2
let g:pad#dir = "C:\\Users\\ratta\\OneDrive\\Dokumente\\notes"
let g:pad#default_format = "votl"
let g:pad#open_in_split = 0
let g:pad#search_backend = "ag"
let g:pad#title_first_line = 1

" Ale {{{2
let g:ale_linters = {'rust':['rls']}
let g:ale_compleion_delay = 120


" rust {{{3
let g:ale_rust_rls_toolchain = 'nightly'
