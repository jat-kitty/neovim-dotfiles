#!/bin/bash

cd bundle || exit 1
rm -rf ./*

git clone https://github.com/tpope/vim-fugitive
git clone https://github.com/ctrlpvim/ctrlp.vim
git clone https://github.com/chriskempson/base16-vim
git clone https://github.com/vim-scripts/brainfuck-syntax
git clone https://github.com/itchyny/lightline.vim
git clone https://github.com/godlygeek/tabular
git clone https://github.com/tpope/vim-endwise
git clone https://github.com/farmergreg/vim-lastplace
git clone https://github.com/fmoralesc/vim-pad
git clone https://github.com/cespare/vim-toml
git clone https://github.com/insanum/votl
git clone https://github.com/arcticicestudio/nord-vim
